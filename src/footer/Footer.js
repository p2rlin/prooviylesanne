import React, { Component } from 'react';
import './Footer.css';

import footer_logo from '../assets/logo.png';
import arrow_icon from '../assets/arrow.svg';
import facebook_icon from '../assets/facebook.png';
import twitter_icon from '../assets/twitter.png';
import youtube_icon from '../assets/youtube.png';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: {
          title: "Kontakt",
          phone: "(+372) 55 123 456",
          open: "Äripäevadel 8:00-16:00",
          email: "info@projektihaldus.eu"
      },
      help: {
          title: "Abi ja tugi",
          links: [
              "Korduvad küsimused",
              "Teata probleemist",
              "Kasutustingimused",
              "Privaatsus"
          ]
      },
      social: {
          title: "Sotsiaalmeedia",
          links: [
              {icon: facebook_icon},
              {icon: twitter_icon},
              {icon: youtube_icon}
          ],
          feedbackLink: "Anna meile tagasisidet"
      }
    }
  }

  displaySocialLinks() {
    return this.state.social.links.map((link, i) => (
        <div className="social__link" key={i}>
            <img src={link.icon} alt=""/>
        </div>
    ));
  }

  render() {
    return (
      <footer>
          <div className="footer__inner">
            <div className="footer__logo">
                <img src={footer_logo} alt=""/>
            </div>
            <div className="footer__contact">
                <div className="contact__title">{this.state.contact.title}</div>
                <div className="contact__phone">{this.state.contact.phone}</div>
                <div className="contact__open">{this.state.contact.open}</div>
                <div className="contact__email">{this.state.contact.email}</div>
            </div>
            <div className="footer__help">
                <div className="help__title">{this.state.help.title}</div>
                {
                    this.state.help.links.map((link, i) => (
                        <div className="help__link" key={i}>
                            <img src={arrow_icon} alt="" className="link__arrow"/> {link}
                        </div>
                    ))
                }
            </div>
            <div className="footer__social">
                <div className="social__title">{this.state.social.title}</div>
                <div className="social__links">
                    {this.displaySocialLinks()}
                </div>
                <div className="feedback__link">
                    <img src={arrow_icon} alt="" className="link__arrow"/> {this.state.social.feedbackLink}
                </div>
            </div>        
          </div>
      </footer>
    );
  }
}

export default Footer;
