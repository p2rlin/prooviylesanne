import React, { Component } from 'react';
import './WarningMessage.css';

import close_icon from '../assets/close_message.svg';

class WarningMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      text: {
        __html: 'Seoses plaanilise uuendamisega <b>20.12.2014</b> ajavahemikus <b>16:30-18:00</b> on rakendusele ligipääs piiratud.'
      }
    }
  }

  close() {
    this.setState({visible: false});
  }

  render() {
    return (
        <div className={"warning__message " + (this.state.visible ? "message--visible" : "message--hidden")}>
        <div className="message__text" dangerouslySetInnerHTML={this.state.text}></div>
        <button className="message__close_btn" onClick={() => this.close()}>
          <img src={close_icon} alt=""/>
        </button>
      </div>
    );
  }
}

export default WarningMessage;
