import React, { Component } from 'react';
import './Content.css';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { SingleDatePicker } from 'react-dates';

import Select from '../select/Select';
import HelpButton from '../help-btn/HelpButton';
import FormRow from '../form-row/FormRow';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountOptions: ["1234567890987654321", "6666666666666666666"],
            users: [
                {name: "Heli Kopter", role: "Projektijuht"},
                {name: "Piia Pliiats", role: "Kujundaja"},
                {name: "Ruuben Ruubionreils", role: "Arendaja"},
            ],
            startDate: null,
            endDate: null,
            focusedInput: null
        }
    }

    displayFormRows() {
        return this.state.users.map((user, i) =>
            (<FormRow key={i} 
                index={i}
                onClick={() => this.removeFormRow(i)} 
                name={user.name}
                role={user.role}
                updateName={this.updateUserName.bind(this)} 
                updateRole={this.updateUserRole.bind(this)}/>));
    }

    updateUserName(name, index) {
        let users = this.state.users;
        users[index].name = name;  

        this.setState({users});
    }

    updateUserRole(role, index) {
        let users = this.state.users;
        users[index].role = role;  

        this.setState({users});
    }

    addFormRow() {
        let newUser = {name: "", role: ""};
        let users = this.state.users;
        users.push(newUser);

        this.setState({users});
    }

    removeFormRow(index) {
        let users = this.state.users;
        users.splice(index, 1);  

        this.setState({users});
    }

    render() {
        return (
        <div className="content">

            <div className="form-group">
                <div className="form-group__title">Klient</div>
                <div className="form-group__control">
                    <label htmlFor="client-company__input">Ettevõte</label>
                    <input  type="text" 
                            className="form-group__input input--big" 
                            id="client-company__input"/>
                </div>
                <div className="form-group__control">
                    <label>Arvelduskonto</label>
                    <Select options={this.state.accountOptions}/>
                </div>
            </div>

            <div className="form-group">
                <div className="form-group__title">Vastutaja</div>
                <div className="form-group__control">
                    <label htmlFor="supervisor-name__input">Nimi</label>
                    <input  type="text" 
                            className="form-group__input input--big" 
                            id="supervisor-name__input"/>
                </div>
                <div className="form-group__control form-group__control--error">
                    <label htmlFor="supervisor-id__input">
                        <span className="error__indicator">!</span>
                        Isikukood
                    </label>
                    <input  type="text" 
                            className="form-group__input input--medium" 
                            id="supervisor-id__input"
                            defaultValue="abc * # ¤ % { } [ ]"/>
                </div>
            </div>

            <div className="form-group">
                <div className="form-group__title">Tähtaeg</div>
                <div className="form-group__control">
                    <div className="date-picker-group">
                    <SingleDatePicker
                        date={this.state.date}
                        onDateChange={date => this.setState({ date })}
                        focused={this.state.focused}
                        onFocusChange={({ focused }) => this.setState({ focused })}
                        displayFormat="DD.MM.YYYY"
                        numberOfMonths={1} 
                        placeholder=""
                        noBorder
                        />

                        <div className="date-picker-hint">(DD.MM.YYYY)</div>
                    </div>
                </div>
            </div>

            <div className="form-group">
                <div className="form-group__title">Kategooria</div>
                <div className="form-group__control">
                    <label className="category__label">
                        <input type="radio" name="category"/>
                        <span className="radio-btn"/>
                        Seiklus- ja meeskonnamängud
                    </label>
                    <label className="category__label">
                        <input type="radio" name="category"/>
                        <span className="radio-btn"/>
                        Elamussaunad, suitsu, parvesaunad, jõesaunad 
                    </label>
                    <label className="category__label">
                        <input type="radio" name="category"/>
                        <span className="radio-btn"/>
                        Spordikeskused ja väljakud (suvel tegutsevad)
                    </label>
                </div>
                <HelpButton/>
            </div>

            <div className="form-group">
                <div className="form-group__title">Kontakt</div>
                <div className="form-group__control">
                    <label>Telefon</label>
                    <div className="controls--inline">
                        <input  type="text" 
                                className="form-group__input input--xsmall input--inline" 
                                placeholder="Kood"/>
                        <input  type="text" 
                                className="form-group__input input--small input--inline" 
                                placeholder="Telefoninumber"/>
                    </div>
                </div>
                <div className="form-group__control">
                    <label>Epost</label>
                    <input type="text" className="form-group__input input--medium"/>
                </div>
                <div className="form-group__control">
                    <label>Skype</label>
                    <input type="text" className="form-group__input input--medium"/>
                </div>
            </div>

            <div className="form-group">
                <div className="form-group__title">Ligipääs</div>
                <div className="form-group__control">
                    <label>Kasutajanimi</label>
                    <input type="text" className="form-group__input input--medium"/>
                </div>
                <div className="form-group__control">
                    <label>Parool</label>
                    <input type="text" className="form-group__input input--medium"/>
                </div>
                <div className="form-group__control">
                    <label>Parool uuesti</label>
                    <input type="text" className="form-group__input input--medium"/>
                </div>
            </div>

            <div className="form-group">
                <div className="form-group__title">Kasutajad</div>
                <div className="form-group__rows">
                    <div className="form-group__row">
                        <label>Nimi</label>
                        <label>Roll</label>
                    </div>
                    {this.displayFormRows()}
                    <button className="add-form-row__btn" onClick={() => this.addFormRow()}>Lisa veel üks</button>
                </div>
            </div>

        </div>
        );
    }
}

export default Content;
