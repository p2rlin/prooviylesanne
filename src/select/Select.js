import React, { Component } from 'react';
import './Select.css';

import caret_icon from '../assets/caret.svg'; 

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      value: ""
    }
  }

  componentDidMount() {
    this.setValue();
  }

  componentDidUpdate() {
    this.setValue();
  }

  setValue() {
    if (this.props.value && this.props.value !== this.state.value) {
      this.setState({value: this.props.value});
    }
  }

  toggleOpen() {
      this.setState({open: !this.state.open});
  }

  selectOption(option) {
      this.setState({value: option});
      if (this.props.updateValue) {
        this.props.updateValue(option);
      }
  }

  render() {
    return (
      <div className="select" onClick={() => this.toggleOpen()}>
        <div className="select__value">{this.state.value}</div>
        <img className="select__arrow" src={caret_icon} alt=""/>
        <div className={"select__options " + (this.state.open ? 'select__options--open' : 'select__options--closed')}>
            {
                this.props.options.map((option,i) => (
                    <div className="option" key={i} onClick={() => this.selectOption(option)}>{option}</div>
                ))
            }
        </div>
      </div>
    );
  }
}

export default Select;
