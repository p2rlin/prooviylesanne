import React, { Component } from 'react';
import './SendButton.css';

import caret_icon from '../assets/caret.svg';

class SendButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
        open: false,
        options: ["Tõlkijale", "Toimetajale", "Tellijale tagasi"]
    }
  }

  toggleOpen() {
      this.setState({open: !this.state.open});
  }

  displayMenu() {
      if (!this.state.open) return;
      return (
        <div className="send-button__menu">
            {this.state.options.map((option, i) => (
                <div className="send-button__option" key={i} onClick={() => this.toggleOpen()}>{option}</div>
            ))}
        </div>
      )
  }

  render() {
    return (
        <div className="send-button__wrapper">
            <button className={"send-button " + (this.state.open ? "send-button--open" : "send-button--closed")}
                    onFocus={() => this.toggleOpen()}
                    onBlur={() => this.toggleOpen()}>
                Saada
                <img src={caret_icon} alt=""/>
            </button>
            {this.displayMenu()}
        </div>
    );
  }
}

export default SendButton;
