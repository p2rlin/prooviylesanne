import React, { Component } from 'react';

import Header from './header/Header';
import Nav from './nav/Nav';
import Title from './title/Title';
import Content from './content/Content';
import Footer from './footer/Footer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  render() {
    return (
      <div className="app">
        <Header/>
        <Nav/>
        <div className="page app__page">
          <Title/>
          <Content/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
