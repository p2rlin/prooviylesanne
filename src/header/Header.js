import React, { Component } from 'react';
import './Header.css';

import Search from '../search/Search';
import Notifications from '../notifications/Notifications';

import logo from '../assets/logo.svg';
import settings_icon from '../assets/gear.svg';
import logout_icon from '../assets/x.svg';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logoText: "Projektihaldus",
            settingsText: "Seaded",
            logoutText: "Logi välja",
            user: {
                name: "Ragnar Rahaboss",
                company: "Nortal AS"
            }
        }
    }

    render() {
        return (
        <header className="header app__header">
            <div className="header__inner">
                <div className="logo">
                    <img className="logo__img" src={logo} alt={this.state.logoText}/>
                    <h1 className="logo__text">{this.state.logoText}</h1>
                </div>

                <Search/>
                
                <div className="user">
                    <div className="user__text">
                        <div className="user__text--name">{this.state.user.name}</div>
                        <div className="user__text--company">{this.state.user.company}</div>
                    </div>
                    <div className="user__btns">
                        <button className="btn__settings">
                            <img    className="btn__settings__img" 
                                    src={settings_icon} 
                                    alt={this.state.settingsText}/>
                        </button>

                        <Notifications/>
                        
                        <button className="btn__logout">
                            <img    className="btn__logout__img" 
                                    src={logout_icon} 
                                    alt={this.state.logoutText}/>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        );
    }
}

export default Header;
