import React, { Component } from 'react';
import './Search.css';
import search_icon from '../assets/luup.svg';
import search_focus_icon from '../assets/luup2.png';
import suggestion_chevron from '../assets/suggestion_chevron.svg';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            placeholder: "Otsi projekti, klienti või kasutajat",
            clients: ["Provision Holding Inc"],
            projects: ["Prooviülesanne", "Virtuaalne proovikabiin", "Contract Approvals"],
            users: ["Ragnar Rahaboss"],
            suggestions: [{title: "Kliendid", values: []}, {title: "Projektid", values: []}, {title: "Kasutajad", values: []}]
        }
    }

    toggleSearchActive() {
        this.setState({active: !this.state.active});
    }

    search(arr, input) {
        return arr.filter(item => 
            item.toLowerCase().search(input.toLowerCase()) !== -1
        );
    }

    getSuggestions(input) {
        let suggestions = [
            {
                title: "Kliendid", 
                values: this.search(this.state.clients, input)
            }, 
            {
                title: "Projektid", 
                values: this.search(this.state.projects, input)
            }, 
            {
                title: "Kasutajad", 
                values: this.search(this.state.users, input)
            }
        ];
        this.setState({suggestions});
    }

    getSuggestionMarkUp(suggestions, key) {
        if (suggestions.values.length === 0) return;
        return (
            <div className="suggestions" key={key}>
                <div className="suggestion__title">{suggestions.title}</div>
                {
                    suggestions.values.map((suggestion, i) => (
                        <div className="suggestion" key={i}>
                            {suggestion}
                            <img className="suggestion__chevron" src={suggestion_chevron} alt=""/>
                        </div>
                    ))
                }
            </div>
        )
    }

    displaySuggestions() {
        const reducer = (accumulator, currentValue) => accumulator + currentValue.values.length;
        let suggestionCount = this.state.suggestions.reduce(reducer, 0);

        if (!this.state.active || suggestionCount === 0) return;
        return (
            <div className="search__suggestions">
                {
                    this.state.suggestions.map((suggestions, i) => this.getSuggestionMarkUp(suggestions, i))
                }
            </div>
        )
    }

    render() {
        return (
            <div className="search__wrapper">
                <div className={"search " + (this.state.active ? "search--active" : "search--inactive")}>
                    <img src={this.state.active ? search_focus_icon : search_icon} className="search__icon" alt=""/>
                    <input  className="search__input" 
                            onFocus={() => this.toggleSearchActive()}
                            onBlur={() => this.toggleSearchActive()}
                            onChange={(e) => this.getSuggestions(e.target.value)}
                            type="text" 
                            placeholder={this.state.placeholder}/>
                </div>
                {this.displaySuggestions()}
            </div>
        );
    }
}

export default Search;
