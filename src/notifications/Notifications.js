import React, { Component } from 'react';
import './Notifications.css';

import notifications_icon from '../assets/bell.png';
import notifications_open_icon from '../assets/bell2.png';
import message_chevron from '../assets/suggestion_chevron.svg';

class Notifications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            tabs: ["Teavitused","Sõnumid"],
            activeTab: 1,
            messages: [
                {
                    text: "Lorem ipsum dolor sit amet",
                    date: "10. oktoober 2014",
                    read: false
                },
                {
                    text: "Curabitur id tellus ac arcu porta sollicit...",
                    date: "22. september 2014",
                    read: true
                },
                {
                    text: "Pellentesque varius pellentesque",
                    date: "9. september 2014",
                    read: true
                }
            ],
            notes: [
                {
                    text: "Teavitus 1",
                    date: "10. oktoober 2014",
                    read: true
                },
                {
                    text: "Teavitus 2",
                    date: "22. september 2014",
                    read: true
                }
            ],
            notifications: [],
            seeAllText: "Vaata kõiki teavitusi"
        }
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    // get reference to HTML element containing notifications button and panel
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    // if panel open, close when clicking outside the element
    handleClickOutside(event) {
        if (this.state.open && this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.toggleOpen();
        }
    }

    toggleOpen() {
        if (!this.state.open) this.selectTab(1);
        this.setState({open: !this.state.open});
    }

    selectTab(index) {
        this.setState({activeTab: index});
        let notifications = index === 0 ? this.state.notes : this.state.messages;
        this.setState({notifications});
    }

    displayTabs() {
        return this.state.tabs.map((tab, i) => (
            <div    className={"tab " + (this.state.activeTab === i ? "tab--open" : "tab--closed")} 
                    key={i}
                    onClick={() => this.selectTab(i)}>
                {tab}
            </div>
        ))
    }

    displayNotifications() {
        return this.state.notifications.map((message, i) => (
            <div className={"notification__message " + (message.read ? "message--read" : "message--unread")} key={i}>
                <div className="message__preview">
                    <div className="message__text">{message.text}</div>
                    <div className="message__date">{message.date}</div>
                </div>
                <img src={message_chevron} alt=""/>
            </div>
        ))
    }

    displayNotificationsPanel() {
        if (!this.state.open) return;
        return (
            <div className="notifications__panel">
                <div className="panel__tabs">
                    {this.displayTabs()}
                </div>
                <div className="panel__notifications">
                    {this.displayNotifications()}
                </div>
                <div className="panel__see-all">
                    {this.state.seeAllText}
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="notifications__wrapper" ref={this.setWrapperRef}>
                <button className={"notifications__btn " + (this.state.open ? "btn--open" : "btn--closed")}
                        onClick={() => this.toggleOpen()}>
                    <img    className="notifications__btn__img" 
                            src={this.state.open ? notifications_open_icon : notifications_icon} 
                            alt=""/>
                </button>
                {this.displayNotificationsPanel()}
            </div>
        );
    }
}

export default Notifications;