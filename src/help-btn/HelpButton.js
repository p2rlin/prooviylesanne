import React, { Component } from 'react';
import './HelpButton.css';

import help_icon from '../assets/help.png';

class HelpButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
        open: false,
        content: `Abitekst ipsum dolor sit amet, consectetur adipiscing elit. 
        Nunc hendrerit diam sit amet nisi rhoncus lacinia. Nulla auctor erat 
        non justo volutpat lobortis. Praesent quis vulputate risus. 
        Consectetur euismod urna, at dapibus enim aliquet sed.`
    }
  }

  toggleOpen() {
      this.setState({open: !this.state.open})
  }

  displayHelpPanel() {
      if (!this.state.open) return;
      return (
        <div className="help__panel">{this.state.content}</div>
      )
  }

  render() {
    return (
        <div className="help__wrapper">
            <button className={"help__btn " + (this.state.open ? "help__btn--open" : "help__btn--closed")}
                    onFocus={() => this.toggleOpen()}
                    onBlur={() => this.toggleOpen()}>
                <img src={help_icon} alt=""/>
            </button>
            {this.displayHelpPanel()}
        </div>
    );
  }
}

export default HelpButton;