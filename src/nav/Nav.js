import React, { Component } from 'react';
import './Nav.css';

class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: [
        "Töölaud",
        "Projektid",
        "Kliendid",
        "Kasutajad",
        "Õigused"
      ],
      activeTab: 1
    }
  }

  displayTabs() {
    return this.state.tabs.map((tab, i) => 
      (<div className={"tab " + (this.state.activeTab === i ? "tab--active": "")} key={i}>{tab}</div>)
    );
  }

  render() {
    return (
        <nav className="nav app__nav">
        <div className="nav__inner">
          {this.displayTabs()}
        </div>
      </nav>
    );
  }
}

export default Nav;
