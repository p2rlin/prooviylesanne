import React, { Component } from 'react';
import './Title.css';

import WarningMessage from '../warning-message/WarningMessage';
import SendButton from '../send-btn/SendButton';

import company_icon from '../assets/company.png';
import check_icon from '../assets/check.svg';

class Title extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "Projekt “Prooviülesanne”",
            company: "Nortal AS",
            status: "Aktiivne",
            showMore: "Näita kogu infot",
            printBtnText: "Prindi",
            saveBtnText: "Salvesta",
            lastChange: "Viimati muutis Aleksander Skafander 9. märtsil 2014 kell 10:36"
        }
    }

    render() {
        return (
          <div className="title">
            <WarningMessage/>
            <div className="title__inner">
                <div className="title__text">
                    <h1>{this.state.title}</h1>
                    <div className="title__details">
                        <div className="details__company">
                            <img src={company_icon} alt={this.state.company}/>
                            <div>{this.state.company}</div>
                        </div>
                        <div className="details__status">
                            <img src={check_icon} alt={this.state.status}/>
                            <div>{this.state.status}</div>
                        </div>
                        <button className="details__show_more">{this.state.showMore}</button>
                    </div>
                </div>
                <div className="title__actions">
                    <div className="action__btns">
                        <button className="btn btn__print">{this.state.printBtnText}</button>
                        <SendButton/>
                        <button className="btn btn__save">{this.state.saveBtnText}</button>
                    </div>
                    <div className="action__last_change">{this.state.lastChange}</div>
                </div>
            </div>
        </div>
        );
    }
}

export default Title;
