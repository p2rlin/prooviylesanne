import React, { Component } from 'react';
import './FormRow.css';

import Select from '../select/Select';

import remove_row_icon from '../assets/remove_row.png';

class FormRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: ["Projektijuht", "Kujundaja", "Arendaja"]
    }
  }

  updateSelectValue(value) {
      this.props.updateRole(value, this.props.index);
  }

  render() {  
    return (
        <div className="form-group__row">
            <input  type="text" 
                    className="form-row__input input--big" 
                    value={this.props.name} 
                    onChange={(e) => this.props.updateName(e.target.value, this.props.index)}/>
                    
            <Select options={this.state.roleOptions} 
                    updateValue={this.updateSelectValue.bind(this)} 
                    value={this.props.role}/>
            
            <button className="remove-row__btn" onClick={this.props.onClick}>
                <img src={remove_row_icon} alt="Eemalda rida"/>
            </button>
        </div>
    );
  }
}

export default FormRow;